import * as http from 'http'
import * as debug from 'debug';
import { Connection } from "mongoose";

import app from "./app";

const port = process.env.PORT || 3000
const server = http.createServer(app.express)
app.express.set('port', port)

app.connectDb().then(() => {
  console.log('connected to mongodb')
  server.listen(port)

  server.on('error', (err: NodeJS.ErrnoException):void => {
    debug(`${err.code} ${err.message}`)
  })

  server.on('listing', ():void => {
    debug(`listening on ${port}`)
  })
}).catch((err:Error) => {
  debug(`${err.message}`)
})



