
import { Model } from "mongoose";

export interface IService {
  model: Model<any>
  getAll(): Promise<any>
  get(id): Promise<any>
  create(data): Promise<any>
  update(id, data): Promise<any>
  delete(id): Promise<any>
}

export class Service implements IService {

  model: Model<any>

  getAll(): Promise<any> {
    return this.model.find({}).exec()
  }

  get(id): Promise<any> {
    return this.model.findById(id).exec()
  }

  create(data): Promise<any> {
    return this.model.create(data)
  }

  update(id, data): Promise<any> {
    return this.model.findByIdAndUpdate(id, data).exec()
  }

  delete(id): Promise<any> {
    return this.model.findByIdAndRemove(id).exec()
  }

}