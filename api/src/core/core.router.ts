import { Router as ExpressRouter, Request, Response, NextFunction } from "express";
import * as debug from 'debug'
import { IService } from "./core.service";

export class Router {
  router: ExpressRouter
  service: IService

  constructor() {
    this.router = ExpressRouter()
    this.routes()
  }

  private routes(): void {
    this.router.get('/', this.getAll.bind(this))
    this.router.get('/:id', this.get.bind(this))
    this.router.post('/', this.create.bind(this))
    this.router.put('/:id', this.update.bind(this))
    this.router.delete('/:id', this.delete.bind(this))
  }

  getAll(req: Request, res: Response, next: NextFunction): void {
    this.service.getAll().then((result) => {
      res.json(result)
    }).catch(next)

  }

  get(req: Request, res: Response, next: NextFunction): void {
    const { id } = req.params
    debug(`id: ${id}`)
    this.service.get(id).then((result) => {
      res.json(result)
    }).catch(next)
  }

  create(req: Request, res: Response, next: NextFunction): void {
    const data = req.body
    this.service.create(data).then((result) => {
      res.json(result)
    }).catch(next)
  }

  update(req: Request, res: Response, next: NextFunction): void {
    const { id } = req.params
    const data = req.body
    this.service.update(id, data).then((result) => {
      res.json(result)
    }).catch(next)
  }

  delete(req: Request, res: Response, next: NextFunction): void {
    const { id } = req.params
    this.service.delete(id).then((result) => {
      res.json(result)
    }).catch(next)
  }

}