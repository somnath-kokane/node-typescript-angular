
import * as express from 'express'
import * as bodyParser from 'body-parser'
import * as mongoose from 'mongoose'
import * as debug from 'debug'

require('mongoose').Promise = global.Promise

import contactRouter from "./contact/contact.router";

export class App {

  MONGODB_URI = 'mongodb://localhost:27017/contactdb'

  express: express.Application
  router: express.Router

  constructor() {
    this.express = express()
    this.router = express.Router()
    this.routes()
    this.middleware()
  }

  middleware() {
    this.express.use(bodyParser.json());
    this.express.use(bodyParser.urlencoded({ extended: false }));
    this.express.use('/api', this.router)
    this.express.use((err: Error, req: express.Request, res: express.Response, next: express.NextFunction): void => {
      res.send(err.stack)
    })
  }

  connectDb(): mongoose.MongooseThenable {
    return mongoose.connect(this.MONGODB_URI)
  }

  routes() {
    this.router.use('/contact', contactRouter)
  }


}

export default new App()