
import { Router } from "../core/core.router";

import { ContactService } from "./contact.service";

export class ContactRouter extends Router {
   
   private _service:ContactService

   get service(): ContactService {
    if(this._service === undefined){
      this._service = new ContactService()
    }
    return this._service
   }
}

export default new ContactRouter().router