

import { Model } from "mongoose";
import { Service } from "../core/core.service";
import contact from './contact.model'

export class ContactService extends Service {

  private _model: Model<any>

  get model(){
    if(this._model === undefined){
      this._model = contact
    }
    return this._model
  }

}

export default new ContactService()