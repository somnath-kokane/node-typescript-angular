import {Schema, model} from 'mongoose';

const contactSchema = new Schema({
  name: String,
  tel: String,
  email: String
});

const contact = model('contacts', contactSchema);

export default contact;
