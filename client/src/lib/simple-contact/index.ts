import { NgModule } from '@angular/core';

import { SimpleContactForm, SimpleContactList } from './components';

const COMPONENTS = [SimpleContactForm, SimpleContactList]

@NgModule({
  imports: [],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
  providers: [],
})
export class SimpleContactModule { }
