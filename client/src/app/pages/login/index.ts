import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";

@Component({
  selector: 'login-page',
  templateUrl: 'login.html',
  styleUrls: ['login.css']
})

export class LoginPage implements OnInit {
  loginForm: FormGroup
  constructor(
    private fb:FormBuilder,
    private router:Router
    ){
    let pattern = /([^\\x00-\\x1F])\\1{3}/;
    this.loginForm = fb.group({
      username: ['smnthk@gmail.com', Validators.required],
      password: ['1@A', Validators.required]
    })
  }
  ngOnInit() { }

  onSubmit(){
    var data = this.loginForm.value
    sessionStorage.setItem('token', 'token')
    this.router.navigate([''])
  }

}