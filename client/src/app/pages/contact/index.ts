import { Component, OnInit, Injectable } from '@angular/core';
import {DataSource} from '@angular/cdk/collections';
import {Observable} from 'rxjs/Observable';
import 'rxjs/add/observable/of';

import {ContactService  } from "../../services/contact.service";

const data: any[] = [
  {position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H'},
  {position: 2, name: 'Helium', weight: 4.0026, symbol: 'He'}
]

@Component({
  selector: 'contact-page',
  templateUrl: 'contact.html'
})

export class ContactPage {

  displayedColumns = ['name', 'tel', 'email'];
  dataSource: DataSource<any>;

  constructor(
    private service: ContactService
  ){
    this.dataSource = new ContactDataSource(this.service)
  }

}

export class ContactDataSource extends DataSource<any> {
 
  constructor(
    private service: ContactService
  ){
    super()
  }

  /** Connect function called by the table to retrieve one stream containing the data to render. */
  connect(): Observable<any[]> {
    return this.service.getAll();
  }

  disconnect() {}
}