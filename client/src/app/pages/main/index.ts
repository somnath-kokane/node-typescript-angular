import { Component, OnInit, ViewEncapsulation, } from '@angular/core';
import { Router } from "@angular/router";


@Component({
  selector: 'main-page',
  // template: '<router-outlet></router-outlet>',
  templateUrl: 'main.html',
  styleUrls: ['main.css'],
  encapsulation: ViewEncapsulation.None,

})

export class MainPage implements OnInit {

  dark = false;
  navItems = [
    {name: 'Dashboard', route: '/dashboard'},
    {name: 'Contact', route: '/contact'}
  ]
  constructor(
    private router:Router
  ) { }

  ngOnInit() { }

  onLogout(){
    sessionStorage.removeItem('token')
    this.router.navigate(['login'])
  }

}