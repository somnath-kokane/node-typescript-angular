import { BrowserModule } from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from "@angular/forms";
import { ValidatorsModule } from 'ngx-validators'

import { 
  RouterModule, Router, Routes, 
  CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";

import { FlexLayoutModule } from '@angular/flex-layout';
import {
  MdInputModule, MdToolbarModule, MdCardModule, MdButtonModule,
  MdSidenavModule,MdTableModule,
  FullscreenOverlayContainer,OverlayContainer} from '@angular/material'

import { SimpleContactModule } from "lib/simple-contact";
import { AuthGuardService } from "../services/auth-guard.service";

import { DashboardPage } from './dashboard';
import { MainPage } from "./main";
import { LoginPage } from "./login";
import { ContactPage } from "./contact";

const MD_MODULES = [
  MdInputModule, MdToolbarModule, MdCardModule, 
  MdButtonModule, MdSidenavModule, MdTableModule
]

export const PAGES_ROUTES: Routes = [
  { path: '', component: MainPage, canActivate: [AuthGuardService], children: [
      { path: '', redirectTo: 'dashboard', pathMatch: 'full' },
      { path: 'dashboard', component: DashboardPage },
      { path: 'contact', component: ContactPage },
    ]
  },
  { path: 'login', component: LoginPage },
]

@NgModule({
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    ReactiveFormsModule,
    ...MD_MODULES,
    RouterModule.forChild(PAGES_ROUTES),
    SimpleContactModule,
  ],
  exports: [...MD_MODULES, SimpleContactModule],
  declarations: [DashboardPage, MainPage, LoginPage, ContactPage],
  providers: [
    {provide: OverlayContainer, useClass: FullscreenOverlayContainer},
    AuthGuardService
  ],
})
export class PagesModule { }
