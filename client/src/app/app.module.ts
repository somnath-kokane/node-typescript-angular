import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ApplicationRef } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { HttpModule } from "@angular/http";

import { AppComponent } from './app.component';

import { PagesModule, PAGES_ROUTES } from "./pages";
import { ContactService } from "./services/contact.service";

const routes: Routes = [
  { path: '', children: PAGES_ROUTES },
]

@NgModule({
  
  imports: [
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(routes),
    PagesModule
  ],
  declarations: [
    AppComponent
  ],
  providers: [ContactService],
  entryComponents: [AppComponent]
})
export class AppModule { 
  constructor(private _appRef: ApplicationRef) { }

  ngDoBootstrap() {
    this._appRef.bootstrap(AppComponent);
  }
}
