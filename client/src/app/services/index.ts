import { NgModule } from '@angular/core';

import { SimpleContactModule } from 'lib/simple-contact';


@NgModule({
  imports: [SimpleContactModule],
  exports: [],
  declarations: [],
  providers: [],
})
export class ContactModule { }
