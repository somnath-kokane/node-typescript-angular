
import { Injectable } from "@angular/core";
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivate } from "@angular/router";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(
    private router: Router
  ) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    let isLoggedIn: boolean = false
    const token = sessionStorage.getItem('token')

    if (!!token) {
      isLoggedIn = true
    }

    if (isLoggedIn) {
      return true
    }

    this.router.navigate(['login'])
    return false
  }

}