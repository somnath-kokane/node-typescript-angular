import { Injectable } from '@angular/core';
import { Http } from "@angular/http";
import {Observable} from 'rxjs/Observable'
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/observable/of';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/do';

const CONTACT_LIST:any[] = [{
    "_id": 1,
    "name": "TerrenceS.Hatfield",
    "tel": "651-603-1723",
    "email": "TerrenceSHatfield@rhyta.com"
}, {
    "_id": 2,
    "name": "ChrisM.Manning",
    "tel": "513-307-5859",
    "email": "ChrisMManning@dayrep.com"
}, {
    "_id": 3,
    "name": "RickyM.Digiacomo",
    "tel": "918-774-0199",
    "email": "RickyMDigiacomo@teleworm.us"
}, {
    "_id": 4,
    "name": "MichaelK.Bayne",
    "tel": "702-989-5145",
    "email": "MichaelKBayne@rhyta.com"
}, {
    "_id": 5,
    "name": "JohnI.Wilson",
    "tel": "318-292-6700",
    "email": "JohnIWilson@dayrep.com"
}, {
    "_id": 6,
    "name": "RodolfoP.Robinett",
    "tel": "803-557-9815",
    "email": "RodolfoPRobinett@jourrapide.com"
}]

let nextId = CONTACT_LIST.length

@Injectable()
export class ContactService {
  store = new BehaviorSubject<any[]>([])
  stateChanges = this.store.asObservable()

  constructor(
    private http:Http
  ){}


  getAll(){
    // this.setState(CONTACT_LIST)
    // return Observable.of(this.store.value)
    return this._getAll()
  }

  private _getAll(){
    return this.http.get(`/api/contact`)
      .map(res => {
        const result = res.json()
        this.setState(result)
        return result
      })
  }

  get(id){
    const item = this.store.value.find(item => item._id === +id)
    return Observable.of(item)
  }

  create(item){
    item.id = ++nextId
    const state = this.store.value
    state.push(item)
    this.setState(state)
    return Observable.of(item)
  }

  update(id, data){
    return this.get(id).map(item => {
      if(item){
        const state = this.store.value
        Object.assign(item, data)
        this.setState(state)
      }
      return item
    })
  }

  delete(id){
    return this.get(id).map(item => {
      if(item){
        const state = this.store.value
        let idx = state.indexOf(item)
        if(idx !== -1){
          state.splice(idx, 1)
        }
        this.setState(state)
      }
      return item
    })
  }

  setState(state){
    this.store.next([...state])
  }

}